These instructions presume you are starting from a working 3.3.x Shibboleth IdP.

1. Change your primary authentication method in `idp.properties` to `MFA`.

2. Add a Principal to the Duo bean and MFA bean in `/conf/authn/general-authn.xml` with our `https://iam.calstate.edu/csuconnect/mfa` AuthnContext string as depicted in the example files.
 
3. Then, make sure your MFA script is written to local needs.  An example from the Chancellor's Office is included.
 
4. If necessary, register for Duo by going to their front page, clicking "sign up" and creating an `@yourcsucampus.edu` account, if not done already.  Then, go to `Applications` and add `Shibboleth`.  Your new keys will be on that page.

5. Configure `duo.properties` in `/conf/authn/`.  It's pretty self-explanatory.  If you are using configuration files older than those distributed with `3.3.0` or the `CSU Shibboleth Template`, you may need to add a pointer to `duo.properties` from `idp.properties`.
