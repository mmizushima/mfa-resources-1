These instructions will permit you to integrate Shibboleth and CAS and add support for MFA for CO resources in one handy guide.

`shibduo.md` will allow Shibboleth to prompt for Duo.  This is generally preferred because you will be able to integrate with more SAML services and present more accurate authentication information to them.

`casduo.md` will allow Shibboleth to rely on CAS to prompt for Duo.  This is generally not preferred because it will require more manual configuration indefinitely, but it's a quicker setup.