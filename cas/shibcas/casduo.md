This guide contains the essential steps necessary to integrate Shibboleth IdP v3.3+ with CAS Server with a Duo MFA overlay.  This guide will result in CAS prompting the user for Duo authentication.  If you would like Shibboleth to prompt, please see `shibduo.md`.

It's not possible for CAS to tell Shibboleth how a user authenticated in order to relay that to the SP.

These steps may impact your existing implementation.  Since these servers can be configured in a thousand ways, it's impossible to document all possible points of contact.  If you have questions, please contact the Chancellor's Office or an appropriate open source list.

It is presumed that you already have a functional Shibboleth environment and CAS environment, but that the two have not been integrated nor configured for MFA.

Please download the Shibcas distribution from [https://github.com/Unicon/shib-cas-authn3/releases](https://github.com/Unicon/shib-cas-authn3/releases).

**If you would like CAS to prompt the user, follow these steps.**

1.  Install the CAS MFA Overlay and enable Duo support following the steps referenced in the CAS section.

2.  Copy the files from Shibcas to the corresponding directories.

        cp -r edit-webapp /opt/shibboleth-idp/edit-webapp
        cp -r flows /opt/shibboleth-idp/flows

3.  Change the authentication mechanism to `Shibcas` in `/opt/shibboleth-idp/conf/idp.properties`.

4.  Add Shibcas properties to `idp.properties`.  These will depend on your CAS server.  **Be sure to know whether your CAS server supports the CAS 3.0 protocol.**

        shibcas.serverName = https://idp.calstate.edu
        shibcas.casServerUrlPrefix = https://cas.calstate.edu/cas
        shibcas.casServerLoginUrl = ${shibcas.casServerUrlPrefix}/login
        shibcas.entityIdLocation = embed
        #If your CAS server only supports CAS 2.0, force the plugin to downversion
        #shibcas.ticketValidatorName = cas20

5.  Turn off session management in the IdP in `idp.properties` to allow CAS to be the single point of session management.  This is not strictly necessary, but neither is sanity.

        # Set to false to disable the IdP session layer
        idp.session.enabled = false

6.  Add a matching rule to your CAS configuration to force MFA for the relayed entityID `https://login.calstate.edu/mfa`.

        https://shibserver.example.edu/idp/Authn/ExtCas\?conversation=[a-z0-9]*&entityId=https://login.calstate.edu/mfa

7.  You can test with a sample resource by going to [https://sp.test.calstate.edu/mfatest](https://sp.test.calstate.edu/mfatest) and selecting your identity provider.

7.  You can test with CFS by going to [https://login.calstate.edu/csuconnect/cfsmfa.asp?env=FCFSPRD](https://login.calstate.edu/csuconnect/cfsmfa.asp?env=FCFSPRD) and selecting your identity provider.
